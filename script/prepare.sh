#!/bin/bash

set -x
set -e

sudo localedef -c -f UTF-8 -i en_US en_US.UTF-8
export LANG='en_US.UTF-8'
export LC_ALL=en_US.UTF-8
source /home/jenkins/.bashrc
export PATH=$HOME/.local/bin:$PATH
sudo yum install -y python3-pip libselinux-python3 iproute
sudo pip3 install --upgrade pip
pip3 install --user -r requirements.txt
which ansible-galaxy
/home/jenkins/.local/bin/ansible-galaxy install bertvv.bind bertvv.rh-base
echo -e '[dns1]\nlocalhost ansible_connection=local' > hosts
ansible localhost -m ansible.builtin.setup
ansible-playbook  -i hosts -C playbook/ci.yml
